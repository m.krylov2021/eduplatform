package com.ua.educationitplatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EducationitplatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(EducationitplatformApplication.class, args);
	}

}
